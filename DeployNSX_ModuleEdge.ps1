﻿$JSONfile = ".\DeployNSX_ModuleEdge.json"

$NSXSpec = $jsondata = Get-Content -Raw -Path $JSONfile | ConvertFrom-Json
$verboseLogFile = "VNF-Deploy.log"
$NSXConnect = Connect-NsxServer -vCenterServer $NSXSpec.VCSAFQDN -Username $NSXSpec.VCSAUser -Password $NSXSpec.VCSAPwd

#### Function to return if a Logical Switch or dvPG Exist ($IntExist = 1) 
Function My-Logger {
    param(
    [Parameter(Mandatory=$true)]
    [string]$status,
    [String]$message,
    [string]$color
    )

    $timeStamp = Get-Date -Format "MM-dd-yyyy_hh:mm:ss"

    Write-Host -NoNewline -ForegroundColor White "[$timestamp]"
    Write-Host -ForegroundColor $color "$status $message"
    $logMessage = "[$timeStamp] $status $message"
    $logMessage | Out-File -Append -LiteralPath $verboseLogFile
}

Function create-DVPG {
    Param(
    [Parameter(Mandatory=$True)]
    [string]$DVswitch,
    [String]$IntName,
    [String]$vlan_ts,
    [String]$CP_Mode,
    [String]$type
    )

if($type -eq "VLAN")
{
        if(Get-VDPortgroup -VDSwitch $DVswitch | where {$_.Name -eq $IntName})
        {
        write-host -ForegroundColor Yellow "dvPG already exist"
        }else{
            try{   
                $newPG = get-vdswitch $DVswitch | New-VDPortgroup -Name $IntName -VlanId $vlan_ts -ErrorAction stop
                My-Logger "Success" "PortGroup  $IntName successfully created" green
                }catch{
                My-Logger "Error" "Unable to create new dvPortGroup $IntName reason == $PSItem" yellow
            }
        }
}

if($type -eq "VXLAN")
{
       try{
            if((get-nsxlogicalswitch -name $IntName).name -eq $IntName){
            My-Logger "Warning" "LS  $IntName Already exist" yellow
            }else{
                $newLS = get-nsxtransportzone -name $vlan_ts | new-nsxlogicalswitch -Name $IntName -ControlPlaneMode $CP_Mode
                My-Logger "Success" "LS  $IntName successfully created" green
                }
            }catch{
                My-Logger "Error" "Unable to create new Logical Switch $IntName reason == $PSItem" yellow
        }
}
  
}

################ Lookup dvPG in order to return either PG or LS object #####################
Function lookup-DVPG {
    Param(
    [Parameter(Mandatory=$True)]
    [String]$IntName
    )
    if([bool](Get-NsxLogicalSwitch -name $IntName) -eq $true){return Get-NsxLogicalSwitch -name $IntName}
    if([bool](Get-VDPortgroup -name $IntName) -eq $true){return Get-VDPortgroup -name $IntName}
}

#########  Create DVPG ########
    foreach($pg in $NSXSpec.dvPG)
        {
        create-DVPG $pg.vDS $pg.name $pg.vlan_ts $pg.CP_Mode $pg.type 
        }

#########  Deploy Edge and Configure Interfaces, routing, Firewall, ECMP ########
    $x=0
    foreach($Edge in $NSXSpec.edges)
        {
        $newEdge = ($Edge.Name).toString()
        My-Logger "Start-task" "Start creating Edge $newEdge" green
         ############## Create new Edge connected to the 1st interface ############
        $resourcePool = Get-ResourcePool -Name $NSXSpec.edges[$x].ResourcePool   #should be resources if we select the master RP
        $datastore = get-datastore -name $NSXSpec.edges[$x].Datastore  
        
         ############## Create 1st interface spec ############################"
    try
        {
            $IntSpecName = $NSXSpec.edges[$x].interfaces[0].ConnectedTo
            $VMNIC0 = New-NsxEdgeInterfaceSpec -Name $NSXSpec.edges[$x].interfaces[0].ConnectedTo -Type $NSXSpec.edges[$x].interfaces[0].connection -ConnectedTo (Get-VDPortgroup -name $NSXSpec.edges[$x].interfaces[0].ConnectedTo) -PrimaryAddress $NSXSpec.edges[$x].interfaces[0].PrimaryAddress -SubnetPrefixLength $NSXSpec.edges[$x].interfaces[0].SubnetPrefixLength -index 0
            My-Logger "Success" "New Interface Spec $IntSpecName for Edge $newEdge" green
        }
    catch{
            My-Logger "Error" "Unable to create new Interface Spec $IntSpecName for Edge $newEdge reason == $PSItem" yellow
        }

    try{
            $newEdge = new-nsxedge -Name $NSXSpec.edges[$x].Name -ResourcePool $resourcePool -Datastore $datastore -Username $NSXSpec.edges[$x].Username -Password $NSXSpec.edges[$x].Password -FormFactor $NSXSpec.edges[$x].FormFactor -FwEnabled:([bool]($NSXSpec.edges[$x].FwEnabled -eq "true")) -Interface $VMNIC0
            My-Logger "Success" "Edge $newEdge successfully created" green
        }
    catch{
            My-Logger "Error" "Unable to create new Edge $newEdge reason == $PSItem" yellow
        }
        ############## connect All interfaces to the Edges ############
        for($i=1; $i -ne $Edge.interfaces.count; $i++)    #Loop for each interfaces and add it to the Edge start from the 2nd interfaces as the first is already connected
            {
                try{
                    ####### Invoke lookup-DVPG in order to return either Logical Switch or dvPortGroup object ###### 
                        $newInt = get-nsxedge -name $NSXSpec.edges[$x].Name | Get-NsxEdgeInterface -index $i | Set-NsxEdgeInterface -name $NSXSpec.edges[$x].interfaces[$i].name -Type $NSXSpec.edges[$x].interfaces[$i].connection -ConnectedTo (lookup-DVPG $NSXSpec.edges[$x].interfaces[$i].connectedTo) -PrimaryAddress $NSXSpec.edges[$x].interfaces[$i].PrimaryAddress -SubnetPrefixLength $NSXSpec.edges[$x].interfaces[$i].SubnetPrefixLength -confirm:$false | out-null
                        $int = $NSXSpec.edges[$x].interfaces[$i].name
                        My-Logger "Success" "Interface $int successfully Added to the edge $newEdge" green
                    }
                catch{
                        My-Logger "Error" "Unable to add new Interface $int to Edge $newEdge reason == $PSItem" yellow
                    }
            }
        $x++
    }

        ############## Configure Routing Only BGP is working for this release############
        foreach($Edge in $NSXSpec.edges)
        {
            try{
                    if($Edge.routingBGP.enableBGP -eq "true"){
                    Get-NsxEdge $Edge.Name | Get-NsxEdgeRouting | Set-NsxEdgeRouting -EnableBgp -RouterId ($Edge.routingBGP.RouterID).tostring() -localAS ($Edge.routingBGP.localAS).toString() -confirm:$false | out-null
                    My-Logger "Success" "bgp Enabled for edge $newEdge" green
                    }
                }
            catch{
                    My-Logger "Error" "Unable to enable BGP for Edge $newEdge reason == $PSItem" yellow
                }

            try{
                    if($Edge.routingBGP.EnableECMP -eq "true"){
                    Get-NsxEdge $Edge.Name | Get-NsxEdgeRouting | Set-NsxEdgeRouting -EnableECMP -confirm:$false | out-null
                    My-Logger "Success" "ECMP Enabled for edge $newEdge" green
                    }
                }
            catch{
                    My-Logger "Error" "Unable to enable ECMP for Edge $newEdge reason == $PSItem" yellow
                }
            try{
                    if($Edge.routingBGP.EnableRouteRedistribution -eq "true"){
                    Get-NsxEdge $Edge.Name | Get-NsxEdgeRouting | Set-NsxEdgeRouting -EnableBgpRouteRedistribution -confirm:$false | out-null
                    My-Logger "Success" "Route Redistribution Enabled for Edge $newEdge" green
                    }
                }
            catch{
                    My-Logger "Error" "Unable to enable route redistribution for Edge $newEdge reason == $PSItem" yellow
                }
            try{
                    Get-NsxEdge $Edge.Name | Get-NsxEdgeRouting | new-NsxEdgeRedistributionRule -Learner bgp -FromConnected -confirm:$false | out-null
                    My-Logger "Success" "Route Redistribution BGP from Connected for Edge $newEdge" green
                 }
            catch{
                    My-Logger "Error" "Unable to enable route redistribution form connected for Edge $newEdge reason == $PSItem" yellow
                }
        ############# Add BGP Neighbour and route Redistribution ####################
        foreach($nei in $Edge.routingBGP.BGPNeighbours)
            {
            try{
                    get-nsxedge $Edge.Name | Get-NsxEdgeRouting | New-NsxEdgeBgpNeighbour -IpAddress ($nei.Nei).tostring() -RemoteAS ($nei.Remote_AS).tostring() -confirm:$false | out-null
                    $newnei = $nei.Nei
                    My-Logger "Success" "Remote Neighbour $newnei added for edge $newEdge" green
            }
        catch{
                    My-Logger "Error" "Unable to add new Neighbour $newnei for Edge $newEdge reason == $PSItem" yellow
            }
        
        }
 
}
My-Logger "End-task" "Finish creating Edges" green   
#########  END Deploy Edge #######################################################################

#########  Deploy and configure DLR ##############################################################
$newDLR = $NSXSpec.dlr.name
My-Logger "start-task" "start creating DLR $newDLR" green

        ############## Create 1st interface spec ############################
        ######## Test if the uplink is a VXlan or VLAN backed #######
        $connectedTo = (Get-nsxlogicalswitch -name $NSXSpec.dlr.interfaces[0].ConnectedTo)
        try{
                $intSpecName = $NSXSpec.dlr.interfaces[0].name
                $DLRNIC0 = New-NsxLogicalRouterInterfaceSpec -type Uplink -Name $NSXSpec.dlr.interfaces[0].name -ConnectedTo $connectedTo -PrimaryAddress $NSXSpec.dlr.interfaces[0].PrimaryAddress -SubnetPrefixLength $NSXSpec.dlr.interfaces[0].SubnetPrefixLength
                My-Logger "Success" "NEw Interface Spec $intSpecName added for DLR $newDLR" green
        }catch{
                My-Logger "Error" "Unable to add NEw Interface Spec $intSpecName added for DLR $newDLR reason == $PSItem" yellow
        }

        ############## Create the DLR with the 1st interface spec. The control VM is placed within a Resource Pool, Cluster will be added in the next release ############################
        $resourcePool = Get-ResourcePool -Name $NSXSpec.dlr.ResourcePool   #should be resources if we select the master RP
        $datastore = get-datastore -name $NSXSpec.dlr.Datastore  
        try{
                $dlr = New-NsxLogicalRouter -name $NSXSpec.dlr.name -ManagementPortGroup $connectedTo -interface $DLRNIC0 -resourcePool $resourcePool -datastore $datastore
                $dlrName = $dlr.name
                My-Logger "Success" "DLR $dlrName successfully created" green
        }catch{
                My-Logger "Error" "Unable to create new DLR $newDLR reason == $PSItem" yellow
        }

        ## Adding DLR interfaces after the DLR has been deployed. Start from the Interface N°2
        for($i=1; $i -ne $NSXSpec.dlr.interfaces.count; $i++)    #Loop for each interfaces and add it to the dlr start from the 2nd interfaces as the first is already connected
        {
        try{
                ####### Only VXLAN is supported at this point ###### 
                $int = $NSXSpec.dlr.interfaces[$i].name
                $dlr | New-NsxLogicalRouterInterface -Type Internal -name $NSXSpec.dlr.interfaces[$i].name  -ConnectedTo (get-nsxlogicalswitch -name $NSXSpec.dlr.interfaces[$i].name)  -PrimaryAddress $NSXSpec.dlr.interfaces[$i].PrimaryAddress -SubnetPrefixLength $NSXSpec.dlr.interfaces[$i].SubnetPrefixLength | out-null
                My-Logger "Success" "Interface $int successfully Added to the DLR $newDLR" green
        }
        catch{
                My-Logger "Error" "Unable to create new DLR $newDLR reason == $PSItem" yellow
        }
        }
        
        ######################   DLR Routing configuration ############################"""
        if($NSXSpec.dlr.routingbgp.enableBGP -eq "true"){
        try{
                Get-NsxLogicalRouter -name $NSXSpec.dlr.name | Get-NsxLogicalRouterRouting | Set-NsxLogicalRouterRouting -EnableBgp -RouterId ($NSXSpec.dlr.routingbgp.RouterID).tostring() -localAS ($NSXSpec.dlr.routingbgp.localAS).toString() -confirm:$false | out-null
                My-Logger "Success" "bgp Enabled for DLR $newDLR" green
        }catch{
                My-Logger "Error" "Unable to enable BGP for DLR $newDLR reason == $PSItem" yellow
        }
        try{        
                Get-NsxLogicalRouter -name $NSXSpec.dlr.name | Get-NsxLogicalRouterRouting | Set-NsxLogicalRouterRouting -EnableBgpRouteRedistribution -confirm:$false | out-null
                My-Logger "Success" "bgp Route redistribution Enabled for DLR $newDLR" green
        }catch{
                My-Logger "Error" "Unable to enable BGP Route redistribution for DLR $newDLR reason == $PSItem" yellow
        }

        for($n = 0; $n -ne $NSXSpec.dlr.routingbgp.BGPNeighbours.count; $n++)
            {
            try{
                    Get-NsxLogicalRouter -name $NSXSpec.dlr.name | Get-NsxLogicalRouterRouting | New-NsxLogicalRouterBgpNeighbour -IpAddress ($NSXSpec.dlr.routingbgp.BGPNeighbours[$n].nei).tostring()  -RemoteAS ($NSXSpec.dlr.routingbgp.BGPNeighbours[$n].remote_AS).tostring() -ForwardingAddress ($NSXSpec.dlr.routingbgp.forwardingAddress).tostring() -ProtocolAddress ($NSXSpec.dlr.routingbgp.ProtocolAddress).tostring() -Weight ($NSXSpec.dlr.routingbgp.BGPNeighbours[$n].Weight).tostring() -HoldDownTimer ($NSXSpec.dlr.routingbgp.BGPNeighbours[$n].holdDownTimer).tostring()  -KeepAliveTimer ($NSXSpec.dlr.routingbgp.BGPNeighbours[$n].KeepAliveTimer).tostring() -confirm:$false | out-null
                    $newNei = $NSXSpec.dlr.routingbgp.BGPNeighbours[$n].nei
                    My-Logger "Success" "bgp Neighbour $newNei added  for DLR $newDLR" green

            }catch{
                My-Logger "Error" "Unable to add bgp Neighbour $newNei for DLR $newDLR reason == $PSItem" yellow
            }
            }
        }
            try{
                if($NSXSpec.dlr.routingbgp.EnableECMP -eq "true"){
                Get-NsxLogicalRouter -name $NSXSpec.dlr.name | Get-NsxLogicalRouterRouting | Set-NsxLogicalRouterRouting -EnableECMP -confirm:$false | out-null
                My-Logger "Success" "ECMP Enabled for DLR $newDLR" green
                }
                }
            catch{
                My-Logger "Error" "Unable to enable ECMP for DLR $newDLR reason == $PSItem" yellow
                }

My-Logger "End-task" "Finish creating DLR $newDLR" green

###### Disconnect everything
#Disconnect-NsxServer
