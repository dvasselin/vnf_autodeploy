# VNF_Autodeploy


version 0.1

From JSON input, deploying a full VNF env :
- NSX architecture (DLR, LS, ESG, routing etc...)
- VCD

Pre-requisites :
- PowerNSX
- PowerCLI
- PowerCLI VCD
- JSONFile within the same directory

Usage : 
- Only 1 vCEnter Connection to avoid duplicate root RP (Resources)
- Duplicate PG name is not supported

The JSON could be manipulate here https://jsonformatter.org/json-editor


